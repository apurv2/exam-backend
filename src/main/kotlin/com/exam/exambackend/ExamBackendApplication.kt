package com.exam.exambackend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ExamBackendApplication

fun main(args: Array<String>) {
	runApplication<ExamBackendApplication>(*args)
}
