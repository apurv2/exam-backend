package com.exam.exambackend.auth.models

import java.io.Serializable

data class SecureUser(
    var uid: String? = null,
    var name: String? = null,
    var email: String? = null,
    var isEmailVerified: Boolean? = false,
    var issuer: String? = null,
    var picture: String? = null
) : Serializable
