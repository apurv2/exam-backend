package com.exam.exambackend.auth

import com.exam.exambackend.auth.models.Credentials
import com.exam.exambackend.auth.models.Credentials.CredentialType
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseToken
import lombok.SneakyThrows
import com.exam.exambackend.auth.models.SecureUser
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class SecurityFilter : OncePerRequestFilter() {
    @Throws(ServletException::class, IOException::class)
    override fun doFilterInternal(
            request: HttpServletRequest,
            response: HttpServletResponse,
            filterChain: FilterChain
    ) {
        if (request.method != "OPTIONS") {
            verifyToken(request)
        }
        filterChain.doFilter(request, response)
    }

    @SneakyThrows
    private fun verifyToken(request: HttpServletRequest) {
        val token = request.getHeader("Authorization")
        val firebaseToken = FirebaseAuth.getInstance().verifyIdToken(token)
        setAuthentication(request, firebaseToken, CredentialType.ID_TOKEN, token, getSecureUser(firebaseToken))
    }

    private fun setAuthentication(
            request: HttpServletRequest,
            decodedToken: FirebaseToken,
            type: CredentialType,
            token: String,
            user: SecureUser
    ) {
        val authentication = UsernamePasswordAuthenticationToken(
                user,
                Credentials(type, decodedToken, token), null
        )
        authentication.details = WebAuthenticationDetailsSource().buildDetails(request)
        SecurityContextHolder.getContext().authentication = authentication
    }

    private fun getSecureUser(decodedToken: FirebaseToken): SecureUser {
        return SecureUser(
                decodedToken.uid,
                decodedToken.name,
                decodedToken.email,
                decodedToken.isEmailVerified,
                decodedToken.issuer,
                decodedToken.picture
        )
    }
}
