package com.exam.exambackend.auth

import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import org.springframework.stereotype.Service
import java.io.IOException
import java.io.InputStream
import javax.annotation.PostConstruct


@Service
class FirebaseInitialize {

    @PostConstruct
    @Throws(IOException::class)
    fun initialize() {
        val inputStream: InputStream? = javaClass.classLoader.getResourceAsStream("serviceAccountKey.json")
        val options = FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(inputStream))
                .setDatabaseUrl("https://ownsubs.firebaseio.com")
                .build()
        FirebaseApp.initializeApp(options)
    }
}
