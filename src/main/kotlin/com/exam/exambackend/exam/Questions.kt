package com.exam.exambackend.exam

data class Questions(
        val number: String,
        val question: String,
        val options: List<Option>


)

data class Option(val value: String, val number: Int)
