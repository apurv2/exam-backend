package com.exam.exambackend.auth.models;

import com.google.firebase.auth.FirebaseToken;
import lombok.Data;

@Data
public class Credentials {

	public enum CredentialType {
		ID_TOKEN
	}

	public Credentials(CredentialType type, FirebaseToken decodedToken, String idToken) {
		this.type = type;
		this.decodedToken = decodedToken;
		this.idToken = idToken;
	}

	private CredentialType type;
	private FirebaseToken decodedToken;
	private String idToken;
}
