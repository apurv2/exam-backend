package com.exam.exambackend.exam

data class Results(
        val userId: String,
        val score: Int,
        val userName : String
)
