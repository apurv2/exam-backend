package com.exam.exambackend.exam

import org.springframework.data.mongodb.repository.MongoRepository

interface ResultsRepository : MongoRepository<Results, String> {
    fun findByUserId(userId: String?): List<Results>
}


