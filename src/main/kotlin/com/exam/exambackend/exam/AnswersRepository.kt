package com.exam.exambackend.exam

import org.springframework.data.mongodb.repository.MongoRepository

interface AnswersRepository : MongoRepository<Answers, String>


