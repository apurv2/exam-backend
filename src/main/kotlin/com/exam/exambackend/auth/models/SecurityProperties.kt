package com.exam.exambackend.auth.models

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties("security")
class SecurityProperties {
    var firebaseProps: FirebaseProperties? = null
    var allowCredentials = false
    var allowedOrigins: List<String>? = null
    var allowedHeaders: List<String>? = null
    var exposedHeaders: List<String>? = null
    var allowedMethods: List<String>? = null
    var allowedPublicApis: List<String>? = null
}
