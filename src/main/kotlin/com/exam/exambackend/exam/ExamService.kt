package com.exam.exambackend.exam

import com.exam.exambackend.auth.models.SecureUser
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ExamService {

    @Autowired
    lateinit var questionsRepository: QuestionsRepository

    @Autowired
    lateinit var answersRepository: AnswersRepository

    @Autowired
    lateinit var resultsRepository: ResultsRepository

    fun processExam(answers: List<Answers>, user: SecureUser) {

        val allAnswers = answersRepository.findAll()

        var correctAnswers = 0
        for (i in answers.indices) {

            if (answers[i] == allAnswers[i]) {
                correctAnswers++;
            }
        }

        resultsRepository.save(Results(user.uid!!, correctAnswers, user.name!!))

    }


    fun createQuestions() {

        val options = listOf(
                Option("India", 1),
                Option("USA", 2),
                Option("Sri Lanka", 3),
                Option("Australia", 4))

        val question = Questions("2", "what are you located", options)
        questionsRepository.save(question)

    }

    fun getAllQuestions() = questionsRepository.findAll()

    fun hasUserAttemptedTheExam(user: SecureUser) =
            resultsRepository.findByUserId(user.uid).isNotEmpty()

}
