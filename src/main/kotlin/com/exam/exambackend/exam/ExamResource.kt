package com.exam.exambackend.exam

import com.exam.exambackend.auth.models.SecureUser
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*

@RestController
class ExamResource {

    @Autowired
    private lateinit var examService: ExamService

    @GetMapping("/questions")
    @CrossOrigin
    fun getAllQuestions() = examService.getAllQuestions()

    @GetMapping("/userAttemptedExam")
    @CrossOrigin
    fun hasUserAttemptedTheExam(@AuthenticationPrincipal user: SecureUser) = examService.hasUserAttemptedTheExam(user)

//    @GetMapping("/createQuestions")
//    @CrossOrigin
//    fun getAnswers() = examService.createQuestions()

    @PostMapping("/answers")
    @CrossOrigin
    fun processExam(@RequestBody answers: AnswersDTO, @AuthenticationPrincipal user: SecureUser) {
        examService.processExam(answers.answers, user)
    }

}
